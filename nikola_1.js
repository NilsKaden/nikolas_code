/*Challenge
Create a market. In the market, create 3 cash registers and 7 customers, each customer has a shopping cart with any amount of products inside from 1 to 10, 
products are different from each other and a customer may have more than 1 of the same product.
The 7 customers will go to the 3 cash registers more or less in an equal split and each register should calculate the total amount each customer should pay.
As there is currently a promotion, any product that costs more than 35 will get a 10% discount and any shopping cart which total is more than 100 
will get an additional 10% discount. Display the total of the carts with the $ sign preceding the value.*/

// This is are carts with the item name, price and amount of each item
const cart1 = [
  {name: "watermelon", price: 5, amount: 5},
  {name: "beef fillet", price: 36, amount: 2},
  {name: "durian", price: 8, amount: 3},
  {name: "apple", price: 1, amount: 4},
];

const cart2 = [
  {name: "applepie", price: 8, amount: 1},
  {name: "chicken wings", price: 10, amount: 10},
  {name: "mango", price: 5, amount: 4},
  {name: "chocolate", price: 2, amount: 2},
];

const cart3 = [
  {name: "shrimps", price: 8, amount: 5},
  {name: "chilli", price: 1, amount: 10},
  {name: "coconut", price: 3, amount: 2},
  {name: "banana", price: 2, amount: 2},
];

function itemsBought(cart) {
  cart.map((item, i) => console.log(`${i+1}. You bought ${item.name} with a quantity of: ${item.amount}`));
}

function calculateTotal(cart) {
  let sum = 0;

  cart.map(item => {
    if (item.price > 35) {
      sum += item.price * item.amount * 0.9
    } else {
      sum += item.price * item.amount;
    }
  });

  if (sum > 100) {
    const reducedSum = sum * 0.9;
    console.log(`Your total price would be $${sum} USD. But since we got a discount, your price is: $${reducedSum} USD`);
  } else {
    console.log(`Your total is $${sum} USD`);
  } 
}

const arrayOfCarts = [cart1, cart2, cart3];
arrayOfCarts.map(cart => {
  console.log(itemsBought(cart));
  console.log(calculateTotal(cart))
});